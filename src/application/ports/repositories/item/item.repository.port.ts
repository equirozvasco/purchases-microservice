import {Item, ItemStock} from '../../../../entities/models/item/item.model.entity';
import {
    ItemCreateInput,
    ItemSearchStockInput,
    ItemSearchInput,
    ItemUpdateInput
} from '../../../../entities/schemas/types/item';

import {IRepository} from "../repository.port";

export type ItemDataCreateInput = ItemCreateInput;
export type ItemDataUpdateInput = ItemUpdateInput;

export interface IItemRepository extends IRepository{
    create(input: ItemDataCreateInput): Promise<Item>;
    create<T>(input: ItemDataCreateInput, transactionManager: T): Promise<Item>;
    update(input: ItemDataUpdateInput): Promise<Item>;
    update<T>(input: ItemDataUpdateInput, transactionManager: T): Promise<Item>;
    findById(input: Omit<ItemSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<Item|undefined>;
    findById<T>(input: Omit<ItemSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager?: T): Promise<Item|undefined>;
    findOneBy(input: ItemSearchInput): Promise<Item|undefined>;
    findAllBy(input: ItemSearchInput): Promise<Array<Item>>;
    delete(input: Omit<ItemSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<void>;
    delete<T>(input: Omit<ItemSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager: T): Promise<void>;
    validateStock(input: ItemSearchStockInput): Promise<ItemStock>
    validateStock<T>(input: ItemSearchStockInput, transactionManager: T): Promise<ItemStock>
}