import {Purchase} from '../../../../entities/models/purchase/purchase.model.entity';
import {
    PurchaseCreateInput,
    PurchaseUpdateInput,
    PurchaseSearchInput
} from '../../../../entities/schemas/types/purchase';

import {IRepository} from "../repository.port";

export type PurchaseDataCreateInput = PurchaseCreateInput;
export type PurchaseDataUpdateInput = PurchaseUpdateInput;

export interface IItemRepository extends IRepository{
    create(input: PurchaseDataCreateInput): Promise<Purchase>;
    create<T>(input: PurchaseDataCreateInput, transactionManager: T): Promise<Purchase>;
    update(input: PurchaseDataUpdateInput): Promise<Purchase>;
    update<T>(input: PurchaseDataUpdateInput, transactionManager: T): Promise<Purchase>;
    findById(input: Omit<PurchaseSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<Purchase|undefined>;
    findById<T>(input: Omit<PurchaseSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager?: T): Promise<Purchase|undefined>;
    findOneBy(input: PurchaseSearchInput): Promise<Purchase|undefined>;
    findAllBy(input: PurchaseSearchInput): Promise<Array<Purchase>>;
    delete(input: Omit<PurchaseSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<void>;
    delete<T>(input: Omit<PurchaseSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager: T): Promise<void>;
}