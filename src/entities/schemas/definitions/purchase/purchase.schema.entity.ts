import Joi from 'joi';
import {ItemGeneralOptionsInputSchema} from "../item/item.schema.entity";

export const PurchaseGeneralOptionsInputSchema = Joi.object({

}).meta({className: "PurchaseGeneralOptionsInput"});

export const PurchaseCreateInputSchema = Joi.object({
    data: Joi.object({
        purchaseDate: Joi.date().required(),
        userId: Joi.number().required(),
        items: Joi.array().items().min(1).required(),
    }).required().min(1),
    options: PurchaseGeneralOptionsInputSchema.optional(),
}).meta({className: 'PurchaseCreateInput'});

export const ItemUpdateInputSchema = Joi.object({
    searchBy: Joi.object({
        id: Joi.number().required()
    }).required().min(1),
    data: Joi.object({
        purchaseDate: Joi.date().optional(),
        userId: Joi.number().optional(),
        items: Joi.array().items().min(1).optional(),
    }).required().min(1),
    options: ItemGeneralOptionsInputSchema.optional(),
}).meta({className: 'PurchaseUpdateInput'});

export const PurchaseDeleteInputSchema = Joi.object({
    searchBy: Joi.object({
        id: Joi.number().required()
    }).required(),
}).meta({className: "PurchaseDeleteInput"});

export const PurchaseSearchInputSchema = Joi.object({
    searchBy: Joi.object({
        id: Joi.number().optional(),
        purchaseDate: Joi.date().optional(),
        userId: Joi.number().optional(),
        items: Joi.array().items().min(1).optional(),
    }),
    options: ItemGeneralOptionsInputSchema.optional()
}).meta({className: 'PurchaseSearchInput'});