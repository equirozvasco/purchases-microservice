import Joi from 'joi';

export const ItemGeneralOptionsInputSchema = Joi.object({

}).meta({className: "ItemGeneralOptionsInput"});

export const ItemCreateInputSchema = Joi.object({
   data: Joi.object({
      title: Joi.string().required(),
      description: Joi.string().required(),
      price: Joi.number().required(),
      quantity: Joi.number().required(),
      creatorId: Joi.number().required()
   }).required().min(1),
   options: ItemGeneralOptionsInputSchema.optional(),
}).meta({className: 'ItemCreateInput'});

export const ItemUpdateInputSchema = Joi.object({
   searchBy: Joi.object({
      id: Joi.number().required()
   }).required().min(1),
   data: Joi.object({
      title: Joi.string().optional(),
      description: Joi.string().optional(),
      price: Joi.number().optional(),
      quantity: Joi.number().optional(),
      creatorId: Joi.number().optional()
   }).required().min(1),
   options: ItemGeneralOptionsInputSchema.optional(),
}).meta({className: 'ItemUpdateInput'});

export const ItemDeleteInputSchema = Joi.object({
   searchBy: Joi.object({
      id: Joi.number().required()
   }).required(),
}).meta({className: "ItemDeleteInput"});

export const ItemSearchInputSchema = Joi.object({
   searchBy: Joi.object({
      id: Joi.number().optional(),
      title: Joi.string().optional(),
      description: Joi.string().optional(),
      price: Joi.number().optional(),
      quantity: Joi.number().optional(),
      creatorId: Joi.number().optional()
   }),
   options: ItemGeneralOptionsInputSchema.optional()
}).meta({className: 'ItemSearchInput'});

export const ItemSearchStockInputSchema = Joi.object({
   searchBy: Joi.object({
      id: Joi.number().required()
   }).required().min(1),
   data: Joi.object({
      quantity: Joi.number().required()
   }).required().min(1),
   options: ItemGeneralOptionsInputSchema.optional(),
}).meta({className: 'ItemSearchStockInput'});