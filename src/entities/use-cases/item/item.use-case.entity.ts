import {ItemCreateInput, ItemSearchStockInput, ItemSearchInput, ItemUpdateInput} from '../../schemas/types/item';
import {Item, ItemStock} from "../../models/item/item.model.entity";

export interface IItemUseCase {
    create(input: ItemCreateInput): Promise<Item>;
    create(input: ItemCreateInput, transactionManager: any): Promise<Item>;
    update(input: ItemUpdateInput): Promise<Item>;
    update(input: ItemUpdateInput, transactionManager: any): Promise<Item>;
    delete(id: number): Promise<void>;
    delete(id: number, transactionManager: any): Promise<void>;
    getById(input: Omit<ItemSearchInput, "searchBy"> & {searchBy: {id: number}}): Promise<Item | undefined>;
    getById(input: Omit<ItemSearchInput, "searchBy"> & {searchBy: {id: number}}, transactionManager: any): Promise<Item | undefined>;
    getOneBy(input: ItemSearchInput): Promise<Item | undefined>;
    getManyBy(input: ItemSearchInput): Promise<Array<Item>>;
    validateStock(input: ItemSearchStockInput): Promise<ItemStock>;
    validateStock(input: ItemSearchStockInput, transactionManager: any): Promise<ItemStock>;
}