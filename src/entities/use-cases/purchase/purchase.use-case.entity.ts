import {PurchaseCreateInput, PurchaseUpdateInput, PurchaseSearchInput} from '../../schemas/types/purchase';
import {Purchase} from "../../models/purchase/purchase.model.entity";

export interface IItemUseCase {
    create(input: PurchaseCreateInput): Promise<Purchase>;
    create(input: PurchaseCreateInput, transactionManager: any): Promise<Purchase>;
    update(input: PurchaseUpdateInput): Promise<Purchase>;
    update(input: PurchaseUpdateInput, transactionManager: any): Promise<Purchase>;
    delete(id: number): Promise<void>;
    delete(id: number, transactionManager: any): Promise<void>;
    getById(input: Omit<PurchaseSearchInput, "searchBy"> & {searchBy: {id: number}}): Promise<Purchase | undefined>;
    getById(input: Omit<PurchaseSearchInput, "searchBy"> & {searchBy: {id: number}}, transactionManager: any): Promise<Purchase | undefined>;
    getOneBy(input: PurchaseSearchInput): Promise<Purchase | undefined>;
    getManyBy(input: PurchaseSearchInput): Promise<Array<Purchase>>;
}