export enum FieldType {
    string = "string",
    integer = "integer",
    float = "float",
    enum = "enum",
    json = "json"
}
export enum ServiceAvailableEvents {
    createBase = "createBase",
    updateBase = "updateBase",
    deleteBase = "deleteBase"
}