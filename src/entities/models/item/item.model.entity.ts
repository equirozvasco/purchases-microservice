export interface Item{
    id: number;
    title: string;
    description: string;
    price: number;
    quantity: number;
    creatorId: number;
    active: boolean;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date | null;
}
export interface ItemStock{
    currentStock: number;
    active: boolean;
}