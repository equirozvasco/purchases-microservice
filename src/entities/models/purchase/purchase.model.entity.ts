import {Item} from '../item/item.model.entity';

export interface Purchase{
    id: number;
    purchaseDate: Date;
    userId: number;
    items: Array<Item>;
    //totalPurchase: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date | null
}